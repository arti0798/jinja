from django.http import HttpResponse
from django.shortcuts import render

def home(request):
    return render(request, 'home.html',{'name':'ARTI'})

def add(request):
    v1 = int(request.POST['num1'])
    v2 = int(request.POST['num2'])
    results = v1+v2
    return render(request, 'results.html', {'results': results})