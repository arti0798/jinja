from django.http import HttpResponse
from django.shortcuts import redirect, render
from itertools import chain

from student.models import ClassName, Student, Subject

def student(request):
    # class_obj = ClassName.objects.all() 
    # print(class_obj)
    std_obj = Student.objects.all()
    print(std_obj)
    
    # sub_obj = Subject.objects.all()
    # all_data = []
    # for subObj in sub_obj.values():
    #     for std_data in std_obj.values():
    #         if std_data['id'] == subObj['id']:
    #             std_data['sub_name'] = subObj['sub_name']
    #             std_data['marks'] = subObj['marks']
    #             all_data.append(std_data)
    
    class_obj = ClassName.objects.all()
    all_data = []
    sub_obj = Subject.objects.all()
    for subObj in sub_obj.values():
        for classObj in class_obj.values():
            for std_data in std_obj.values():
                if std_data['id'] == subObj['id'] and std_data['id'] == classObj['id']:
                    std_data['sub_name'] = subObj['sub_name']
                    std_data['marks'] = subObj['marks']
                    std_data['className'] = classObj['class_name']
                    all_data.append(std_data)
                
                
                
    # print("\n================================", all_data)            
      
    return render(request, 'detail.html', {'std':all_data})
 
def check_by_email(request):
    email_id_to_check=''
    if request.method == 'POST':
        email_id_to_check = request.POST.get('email_id_to_check')
    
        std_obj = Student.objects.filter(email_id=email_id_to_check)
        class_obj = ClassName.objects.all()
        all_data = []
        
        # for classObj in class_obj.values():
        #     for student in std_obj.values():
        #         if student['id'] == classObj['id']:
        #             student['className'] = classObj['class_name']
        #             all_data.append(student)
        # print('>>>>>>>>>>>>>',all_data)            
        
        sub_obj = Subject.objects.all()
        for subObj in sub_obj.values():
            for classObj in class_obj.values():
                for std_data in std_obj.values():
                    if std_data['id'] == subObj['id'] and std_data['id'] == classObj['id']:
                        std_data['sub_name'] = subObj['sub_name']
                        std_data['marks'] = subObj['marks']
                        std_data['className'] = classObj['class_name']
                        all_data.append(std_data)
        
        print("\n-------- ", std_obj.values())
    # print(std_obj)
    
    # sub_obj = Subject.objects.all()
    
        return render(request, 'particular_student.html', {'std':all_data})
    else:
        return render(request, 'by_email.html')
    

def submit_data(request):
    
    if request.method == 'POST':
        print("DAT>>>>>>>>>>> ",request.POST['roll_no'])
        roll_no = request.POST['roll_no']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        class_name = request.POST['class_name']
        email_id = request.POST['email_id']
        sub_name = request.POST['sub_name']
        marks = request.POST['marks']
        
        
        try:
            class_data = ClassName.objects.get(class_name=class_name)
        except ClassName.DoesNotExist:
            class_data = ClassName.objects.create(class_name=class_name)
            class_data.save()
            
        try: 
            student_data = Student.objects.get(email_id=email_id)
        except:        
            student_data = Student.objects.create(roll_no=roll_no,first_name=first_name,last_name=last_name,email_id=email_id,class_name=class_data)
            student_data.save()
        
        sub_detail = Subject.objects.create(sub_name=sub_name,marks=marks,student=student_data)    
        print("DATA SAVED>>>>>>")
        return redirect('/student')
    else:
        return render(request, 'student_detail.html')
    
    
    
    
    
    
    
    
    
    
    
    
       # std1 = Student()
    # std1.name = "Anand Singh"
    # std1.roll = 1
    # std1.passed = True
    
    # std2 = Student()
    # std2.name = "Azad Singh"
    # std2.roll = 2
    # std2.passed = False
    
    # std = [std1,std2]
    # return render(request, 'detail.html', {'std':std})
    
# def submit_data(request):
    
#     if request.method == 'POST':
#         print("DAT>>>>>>>>>>> ",request.POST['roll_no'])
#         roll_no = request.POST['roll_no']
#         first_name = request.POST['first_name']
#         last_name = request.POST['last_name']
#         class_name = request.POST['class_name']
#         email_id = request.POST['email_id']
        
#         data = Student.objects.create(roll_no=roll_no,first_name=first_name,last_name=last_name,email_id=email_id)
        
#         data.save()
#         print("DATA SAVED>>>>>>")
#         return redirect('/student')
#     else:
#         return render(request, 'student_detail.html')    