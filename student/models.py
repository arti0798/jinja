from django.db import models


class ClassName(models.Model):
    class_name = models.CharField(max_length=100)
    
    def __str__(self):
        return f'{self.class_name}'
    
class Student(models.Model):
    roll_no = models.IntegerField()
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    class_name = models.ForeignKey(ClassName, on_delete=models.DO_NOTHING, related_name="student_class", null=True, blank=True)
    email_id = models.EmailField(max_length=254)
    def __str__(self):
        return f'{self.first_name}'
    
class Subject(models.Model):
    sub_name = models.CharField(max_length=100)
    student = models.ForeignKey(Student, on_delete=models.DO_NOTHING, related_name="subject_student", null=True, blank=True)
    # marks = models.ManyToManyField(Student, blank=True)
    marks = models.FloatField()
    
    def __str__(self):
        return f'{self.sub_name}'
    
# class Marks(models.Model):
#     student = models.ForeignKey("Student", on_delete=models.DO_NOTHING, null=True, blank=True)
#     subject = models.ForeignKey(Subject, on_delete=models.DO_NOTHING, blank=True, null=True , related_name='marks')            


# class Student:
#     name : str
#     roll : int
#     passed: bool