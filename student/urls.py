from django.urls import path
from . import views

urlpatterns = [
    path('',views.student,name='student'),
    path('submit_data',views.submit_data,name='submit_data'),
    path('check_by_email',views.check_by_email,name='check_by_email'),
] 


