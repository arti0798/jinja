from django.contrib import admin
from .models import Student
from .models import ClassName
from .models import Subject

admin.site.register(ClassName)
admin.site.register(Student)
admin.site.register(Subject)